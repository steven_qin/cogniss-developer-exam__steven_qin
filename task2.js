const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputURL = "http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=";
const outputFile = "output3.json";

var output = {}
console.log("getting 10 acronyms");
output.definitions = [];

console.log("creating looping function")
const getAcronym = function() {
    var acronym = randomstring.generate({
        length: 3,
        charset: 'alphabetic'
    }).toUpperCase();

    got(inputURL+acronym).then(response => {
        console.log("got data for acronym", acronym);
        console.log("add returned data to definitions array");
        // console.log(JSON.parse(response.body)[0]);
        if (!JSON.parse(response.body)[0]){
            output.definitions.push({"sf": acronym, "lfs": []});
        } else {
            output.definitions.push(JSON.parse(response.body)[0]);
        }

        if (output.definitions.length < 10) {
            console.log("calling looping function again");
            getAcronym();
        }
        if (output.definitions.length === 10) {
            console.log("saving output file formatted with 2 space indenting");
            jsonfile.writeFile(outputFile, output, {spaces: 2}, function (err) {
                console.log("All done!");
            });
        }
    }).catch(err => {
        console.log(err)
    })
}

console.log("calling looping function");
getAcronym();

// console.log("saving output file formatted with 2 space indenting");
// jsonfile.writeFile(outputFile, output, {spaces: 2}, function (err) {
//     console.log("All done!");
// });