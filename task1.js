const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputFile = "input2.json";
const outputFile = "output2.json";

let output = {}

const reverse = (s)=>{
    return s.split('').reverse().join('')
}
jsonfile.readFile(inputFile, function (err, body) {
    console.log("loaded input file content", body);
    let emails_output = []
    for (let i=0; i<body.names.length;i++){
        let fake_mail = reverse(body.names[i]) + randomstring.generate(5) + "@gmail.com";
        emails_output.push(fake_mail);
    }
    output.emails = emails_output;
    console.log("generate fake emails output", output);

    jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
        console.log("All done!");
    });
})